package types

type Live struct {
	ClaimId      string
	RelTime			 string
	Time         string
	ThumbnailUrl string
	StreamUrl    string
	Live         bool
}
