module codeberg.org/librarian/librarian

go 1.16

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/gofiber/fiber/v2 v2.23.0
	github.com/gofiber/template v1.6.20
	github.com/gomarkdown/markdown v0.0.0-20210514010506-3b9f47219fe7
	github.com/gorilla/feeds v1.1.1
	github.com/hashicorp/go-retryablehttp v0.7.0 // indirect
	github.com/lucas-clemente/quic-go v0.24.0
	github.com/microcosm-cc/bluemonday v1.0.15
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/spf13/viper v1.8.1
	github.com/tidwall/gjson v1.8.1
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
